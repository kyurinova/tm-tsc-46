package ru.tsc.kyurinova.tm.service.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.*;
import ru.tsc.kyurinova.tm.api.service.dto.ISessionDTOService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIndexException;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.repository.dto.SessionDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.UserDTORepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDTOService implements ISessionDTOService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionDTOService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    @SneakyThrows
    public SessionDTO open(@NotNull final String login, @NotNull final String password) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return sign(session);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    @NotNull
    public SessionDTO sign(@NotNull final SessionDTO session) {
        session.setSignature(null);
        @NotNull final String salt = serviceLocator.getPropertyService().getSessionSecret();
        final int cycle = serviceLocator.getPropertyService().getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable SessionDTO session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public boolean exists(@NotNull final String sessionId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        @NotNull final SessionDTO session1 = sessionRepository.findById(sessionId);
        if (session1 != null) return true;
        else return false;
    }

    @Override
    public void validate(@NotNull final SessionDTO session) {
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final SessionDTO sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            if (!exists(session.getId())) throw new AccessDeniedException();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }

    }

    @SneakyThrows
    @Override
    public void validate(@NotNull SessionDTO session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        @Nullable final UserDTO user = userRepository.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void remove(@Nullable final SessionDTO entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.remove(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            return sessionRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            return sessionRepository.findById(id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            return sessionRepository.findByIndex(index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
        try {
            return sessionRepository.getSize();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

}
